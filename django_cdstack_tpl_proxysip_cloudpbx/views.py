from django.template import engines
from django_cdstack_deploy.django_cdstack_deploy.func import generate_config_static

from django_cdstack_tpl_proxysip.django_cdstack_tpl_proxysip.views import (
    handle as handle_proxysip,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = (
        "django_cdstack_tpl_proxysip_cloudpbx/django_cdstack_tpl_proxysip_cloudpbx"
    )

    generate_config_static(zipfile_handler, template_opts, module_prefix)
    handle_proxysip(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
